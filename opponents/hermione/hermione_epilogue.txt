		<epilogue gender="female">
		<title>Potions Class</title>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>50%</y>
				<width>15%</width>
				<arrow>left</arrow>
				<content>Thanks for meeting me! Did you have any trouble finding the place? It's not on most maps.</content>
			</text>
			<text>
				<x>70%</x>
				<y>70%</y>
				<width>15%</width>
				<arrow>left</arrow>
				<content>Shall we head upstairs? I've rented a room for the night.</content>
			</text>
		</screen>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>30%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>Do you remember that spell I cast at our game?</content>
			</text>
			<text>
				<x>70%</x>
				<y>40%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>I've been doing some research into it. <i>Lots</i> of research, actually. There's a rather large number of books on the subject in the library.</content>
			</text>
			<text>
				<x>70%</x>
				<y>55%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>Which shouldn't surprise anyone, I guess. Boarding school full of teenagers? I'm surprised there's not house orgies!</content>
			</text>
			<text>
				<x>70%</x>
				<y>70%</y>
				<width>20%</width>
				<arrow>left</arrow>
				<content>...Wait, there <i>aren't</i> house orgies, are there?</content>
			</text>
		</screen>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>30%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>Anyway! I've found a particularly promising formula.</content>
			</text>
			<text>
				<x>70%</x>
				<y>40%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>It's a potion which will heighten pleasure by a factor of ten. <i>Ten</i>! Can you imagine?</content>
			</text>
			<text>
				<x>70%</x>
				<y>55%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>You have no idea how badly I want to brew this! But sadly, I'm missing an ingredient. It's not something I can buy. And I can't ask Harry or Ron to help.</content>
			</text>
			<text>
				<x>70%</x>
				<y>75%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>And, um, that's where you come in.</content>
			</text>
		</screen>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>40%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>You see... well... I'll just come out and say it, shall I?</content>
			</text>
			<text>
				<x>70%</x>
				<y>50%</y>
				<width>20%</width>
				<arrow>left</arrow>
				<content>I need 'the juices of two witches, mingled together'.</content>
			</text>
			<text>
				<x>70%</x>
				<y>60%</y>
				<width>20%</width>
				<arrow>left</arrow>
				<content>So... I was thinking... if you promise to help me gather the ingredients, I'll happily test the potion with you. Does that seem fair?</content>
			</text>
		</screen>
	</epilogue>
		<epilogue gender="male">
		<title>Potions Class</title>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>50%</y>
				<width>15%</width>
				<arrow>left</arrow>
				<content>Thanks for meeting me! Did you have any trouble finding the place? It's not on most maps.</content>
			</text>
		</screen>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>30%</y>
				<width>15%</width>
				<arrow>left</arrow>
				<content>So, um... as to why I called you here...</content>
			</text>
			<text>
				<x>70%</x>
				<y>40%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>Do you remember that spell I cast at our game?</content>
			</text>
			<text>
				<x>70%</x>
				<y>50%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>I've been doing some research into it. <i>Lots</i> of research, actually. There's a rather large number of books on the subject in the library.</content>
			</text>
			<text>
				<x>70%</x>
				<y>55%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>Which shouldn't surprise anyone, I guess. Boarding school full of teenagers? I'm surprised there's not house orgies!</content>
			</text>
			<text>
				<x>70%</x>
				<y>70%</y>
				<width>20%</width>
				<arrow>left</arrow>
				<content>...Wait, there <i>aren't</i> house orgies, are there?</content>
			</text>
		</screen>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>30%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>Anyway! I've found a particularly promising formula.</content>
			</text>
			<text>
				<x>70%</x>
				<y>40%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>It's a potion which will heighten pleasure by a factor of ten. <i>Ten</i>! Can you imagine?</content>
			</text>
			<text>
				<x>70%</x>
				<y>55%</y>
				<width>25%</width>
				<arrow>left</arrow>
				<content>You have no idea how badly I want to brew this! But sadly, I'm missing an ingredient. It's not something I can buy. And I don't dare ask Harry to help.</content>
			</text>
			<text>
				<x>70%</x>
				<y>75%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>And, um, that's where you come in.</content>
			</text>
		</screen>
		<screen img="epilogue-temp.png">
			<start>0</start>
			<text>
				<x>70%</x>
				<y>40%</y>
				<width>30%</width>
				<arrow>left</arrow>
				<content>You see... well... I'll just come out and say it, shall I?</content>
			</text>
			<text>
				<x>70%</x>
				<y>50%</y>
				<width>20%</width>
				<arrow>left</arrow>
				<content>I need cum. And it has to be fresh.</content>
			</text>
			<text>
				<x>70%</x>
				<y>60%</y>
				<width>20%</width>
				<arrow>left</arrow>
				<content>So... I was thinking... if you provide the ingredients, I'll happily test the potion with you. Does that seem fair?</content>
			</text>
		</screen>
	</epilogue>
