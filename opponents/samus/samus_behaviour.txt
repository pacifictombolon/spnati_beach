#required for behaviour.xml
first=Samus
last=Aran
label=Samus
gender=female
size=medium
timer=20

tag=blonde
tag=confident
tag=future

#required for meta.xml
pic=0-calm
height=6'3"
from=Metroid Series
writer=stealthsnowpaw
artist=stealthsnowpaw &amp; Horsecatdraws
description=A space-faring bounty hunter.
release=38

#player clothing
clothes=Gloves,gloves,extra,other
clothes=Boots,boots,extra,other
clothes=Bodysuit,bodysuit,major,lower
clothes=Bra,bra,important,upper
clothes=Panties,panties,important,lower

#start image
start=0-calm,I've never played this game, but Peach said it was fun.

swap_cards=calm,I need ~cards~ cards.

#card cases
#fully clothed
0-good_hand=0-happy,Oh... this looks promising!
0-okay_hand=0-sad,Hmmmm. Not what I was hoping for, but not terrible overall.
0-bad_hand=0-loss,Well these cards are outright terrible.

#lost gloves
1-good_hand=1-happy,Oh... this looks promising!
1-okay_hand=1-sad,Hmmmm. Not what I was hoping for, but not terrible overall.
1-bad_hand=1-loss,Well these cards are outright terrible.

#lost boots
2-good_hand=2-happy,Oh... this looks promising!
2-okay_hand=2-sad,Hmmmm. Not what I was hoping for, but not terrible overall.
2-bad_hand=2-loss,Well these cards are outright terrible.

#lost bodysuit
3-good_hand=3-happy,Oh... this looks promising!
3-okay_hand=3-sad,Hmmmm. Not what I was hoping for, but not terrible overall.
3-bad_hand=3-loss,Well these cards are outright terrible.

#lost bra
4-good_hand=4-happy,Oh... this looks promising!
4-okay_hand=4-sad,Hmmmm. Not what I was hoping for, but not terrible overall.
4-bad_hand=4-loss,Well these cards are outright terrible.

#lost panties/naked
5-good_hand=5-happy,Oh... this looks promising!
5-okay_hand=5-sad,Hmmmm. Not what I was hoping for, but not terrible overall.
5-bad_hand=5-loss,Well these cards are outright terrible.




#character must strip
#fully clothed
0-must_strip_winning=0-embarrassed,Oh well. I'm still pretty well off.
0-must_strip_normal=0-sad,Welp, the rules are rules. Here we go.
0-must_strip_losing=0-loss,Welp, the rules are rules. Here we go.
0-stripping=0-strip,I guess my gloves will be the first to go.
1-stripped=1-calm,You weren't really expecting to get me out of my zero suit THAT easily, right?

#lost gloves
1-must_strip_winning=1-loss,Well, it seems like I can afford to lose this one...
1-must_strip_normal=1-loss,Well, here we go.
1-must_strip_losing=1-loss,I guess taking our clothes off is the whole point now, isn't it?
1-stripping=1-strip,I don't have too much to take off after this.
2-stripped=2-calm,If I lose another hand, you'll finally get me out of this zero suit.

#lost boots
2-must_strip_winning=2-calm,I'm not too worried yet. After all, I really wanted to play this game with you.
2-must_strip_normal=2-calm,I'm not too worried yet. After all, I really wanted to play this game with you.
2-must_strip_losing=2-sad,Oh, come on. Isn't one of you going to join me sometime? It would be more interesting that way.
2-stripping=2-strip,You did it! Are you excited to see what's beneath the suit? I might need help getting this off quickly... It's usually a hassle.
3-stripped=3-happy,It's not like it left much to the imagination anyways...

#lost bodysuit
3-must_strip_winning=3-sad,Well I guess it's time I joined everyone else here.
3-must_strip_normal=3-sad,Uh oh, looks like it's a race to see who loses next at this point...
3-must_strip_losing=3-sad,Well it looks like I'll be showing more skin than anyone else for now. Oh well.
3-stripping=3-strip,I think you'll like what I've got under my bra.
4-stripped=4-happy,Well? What do you think? Are they everything you thought they would be?

#lost bra
4-must_strip_winning=4-happy,I may have lost this hand, but I think we're all winning at this point.
4-must_strip_normal=4-happy,I may have lost this hand, but I think we're all winning at this point.
4-must_strip_losing=4-embarrassed,Well I guess you're really getting what you wanted to see when you asked me to play, huh?
4-stripping=4-strip,How many times have you thought about this moment at night? Are you ready?
5-stripped=5-calm,Am I living up to your expectations?



#character masturbation cases

#lost panties/naked
5-must_masturbate_first=5-embarrassed,Hmmm. I'm definitely turned on, but I wanted to watch someone else first...
5-must_masturbate=5-happy,Well it looks like it's time for me to join the real party...
5-start_masturbating=5-starting,Mmmmmmm. I'm not going to lie. Knowing you're all watching me is getting me even more wet.

#masturbating
6-masturbating=6-excited,I'm not going to lie, I really like putting on a show for you.
6-masturbating=6-happy,This feels so good. Maybe one of you could come over here and give me a hand?
6-masturbating=6-horny,Oh my god, I'm getting so wet.
6-heavy_masturbating=6-heavy,Ohhhh. I'm so close. Somebody say something dirty to me.
6-finishing_masturbating=6-finishing,Yes! Yes! That did it! I'm cumming! Are you watching?! OH GOD! Ahhhhhh!

#finished
7-finished_masturbating=7-excited,Cumming with an audience is intense! Maybe I should do this more often?
7-finished_masturbating=7-happy,That was so good, but I just want more. Who wants to go to a bedroom with me? Everyone's invited after the game.


#game over


#finished
7-game_over_defeat=7-horny,This was so much fun, but we're just getting started, right? We're going to have fun all night long...


#other player must strip
#fully clothed
0-male_human_must_strip=0-interested,Rules are rules! Take it off, ~name~!
0-male_must_strip=0-interested,Rules are rules! Take it off, ~name~!
0-female_human_must_strip=0-calm,I'm just glad it's you, ~name~, and not me.
0-female_must_strip=0-calm,I'm just glad it's you, ~name~, and not me.

#lost gloves
1-male_human_must_strip=1-interested,Rules are rules! Take it off, ~name~!
1-male_must_strip=1-interested,Rules are rules! Take it off, ~name~!
1-female_human_must_strip=1-calm,I'm just glad it's you, ~name~, and not me.
1-female_must_strip=1-calm,I'm just glad it's you, ~name~, and not me.

#lost boots
2-male_human_must_strip=2-interested,Rules are rules! Take it off, ~name~!
2-male_must_strip=2-interested,Rules are rules! Take it off, ~name~!
2-female_human_must_strip=2-excited,Yes!
2-female_must_strip=2-interested,Yes!

#lost bodysuit
3-male_human_must_strip=3-excited,Ya! Take it off!
3-male_must_strip=3-excited,Ya! Take it off!
3-female_human_must_strip=3-excited,Yes!
3-female_must_strip=3-excited,Yes! I was hoping you'd lose this hand, ~name~!

#lost bra
4-male_human_must_strip=4-excited,Ya! Take it off!
4-male_must_strip=4-excited,Ya! Take it off!
4-female_human_must_strip=4-excited,Yes!
4-female_must_strip=4-excited,Yes! I was hoping you'd lose this hand, ~name~!

#lost panties/naked
5-male_human_must_strip=5-interested,Yes! I'm naked, so you might as well join me, ~name~.
5-male_must_strip=5-interested,Yes! I'm naked, so you might as well join me, ~name~.
5-female_human_must_strip=5-excited,Yes!
5-female_must_strip=5-excited,Yes!

#masturbating
6-male_human_must_strip=6-interested,Mmmm. Something more appealing to look at is coming my way?
6-male_must_strip=6-interested,Mmmm. Something more appealing to look at is coming my way?
6-female_human_must_strip=6-interested,What will you be taking off?
6-female_must_strip=6-interested,What will you be taking off?

#finished
7-male_human_must_strip=7-interested,Did watching me cum make you hard already? I think I see the bulge in your pants pretty clearly...
7-male_must_strip=7-interested,Did watching me cum make you hard already? I think I see the bulge in your pants pretty clearly...
7-female_human_must_strip=7-shocked,How do you still have clothes left to take off? Just get naked and enjoy the fun!
7-female_must_strip=7-shocked,How do you still have clothes left to take off? Just get naked and enjoy the fun!


#other player is removing an accessory
#fully clothed
0-male_removing_accessory=0-angry,What? That's it?!
0-male_removed_accessory=0-angry,Weak. You can do better than just your ~clothing~.
0-female_removing_accessory=0-angry,What? That's it?!
0-female_removed_accessory=0-angry,Weak. You can do better than just your ~clothing~.

#lost gloves
1-male_removing_accessory=1-angry,What? That's it?!
1-male_removed_accessory=1-angry,Weak. You can do better than just your ~clothing~.
1-female_removing_accessory=1-angry,What? That's it?!
1-female_removed_accessory=1-angry,Weak. You can do better than just your ~clothing~.

#lost boots
2-male_removing_accessory=2-angry,What? That's it?!
2-male_removed_accessory=2-angry,Weak. You can do better than just your ~clothing~.
2-female_removing_accessory=2-loss,Oh, you got me all worked up for nothing.
2-female_removed_accessory=2-loss,I hope you take something more exciting off next, ~name~.

#lost bodysuit
3-male_removing_accessory=3-angry,You're no fun!
3-male_removed_accessory=3-angry,If you don't take off something more interesting next, I'm leaving!
3-female_removing_accessory=3-loss,Oh, you got me all worked up for nothing.
3-female_removed_accessory=3-sad,I think we'd all appreciate it if you were more adventurous next time, ~name~.

#lost bra
4-male_removing_accessory=4-angry,You finally get to see my breasts and this is how you repay me?!
4-male_removed_accessory=4-calm,I'm not going to forgive you for this one.
4-female_removing_accessory=4-loss,Oh, you got me all worked up for nothing.
4-female_removed_accessory=4-calm,I guess I can understand why you'd take it off though.

#lost panties/naked
5-male_removing_accessory=5-angry,Are you kidding me? Do you even want to play the game? Take something better off next time!
5-male_removed_accessory=5-loss,I didn't mean to yell, but you're really disappointing me right now.
5-female_removing_accessory=5-loss,Oh, you got me all worked up for nothing.
5-female_removed_accessory=5-calm,I guess I can understand why you'd take it off though.

#masturbating
6-male_removing_accessory=6-angry,Are you serious?! Such a tease!
6-male_removed_accessory=6-sad,I guess I'll just think about the last time Peach and I spent a night alone...
6-female_removing_accessory=6-sad,Oh, I was hoping you would take something off that would get me closer to finishing...
6-female_removed_accessory=6-calm,You're just trying to make me take longer, aren't you?

#finished
7-male_removing_accessory=7-sad,You're just a buzzkill at this point.
7-male_removed_accessory=7-sad,I can't believe you still have your ~clothing~ to take off...
7-female_removing_accessory=7-shocked,Wow, I didn't realize you haven't lost many hands at all.
7-female_removed_accessory=7-interested,What will you be taking off?


#other player is removing a minor clothing item
#fully clothed
0-male_removing_minor=0-angry,About time!
0-male_removed_minor=0-sad,Awww, I was hoping for something more.
0-female_removing_minor=0-angry,Surely you can do better than that.
0-female_removed_minor=0-calm,Alright, deal the next hand already.

#lost gloves
1-male_removing_minor=1-angry,About time!
1-male_removed_minor=1-sad,Awww, I was hoping for something more.
1-female_removing_minor=1-angry,Surely you can do better than that.
1-female_removed_minor=1-calm,Alright, deal the next hand already.

#lost boots
2-male_removing_minor=2-angry,About time!
2-male_removed_minor=2-sad,Awww, I was hoping for something more.
2-female_removing_minor=2-sad,Can't you be a bit more adventurous, ~name~?
2-female_removed_minor=2-sad,I mean, I don't have anything like that to take off...

#lost bodysuit
3-male_removing_minor=3-angry,You're no fun!
3-male_removed_minor=3-angry,If you don't take off something more interesting next, I'm leaving!
3-female_removing_minor=3-sad,Oh boo! You're no fun, ~name~!
3-female_removed_minor=3-loss,I hope you're more adventurous the next time you lose...

#lost bra
4-male_removing_minor=4-angry,I'm going to cover myself up again if that's all you're gonna take off!
4-male_removed_minor=4-calm,Maybe I'll just take my hairband off next, just to spite you.
4-female_removing_minor=4-sad,Oh boo! You're no fun, ~name~!
4-female_removed_minor=4-interested,I hope you take something more interesting off next time!

#lost panties/naked
5-male_removing_minor=5-angry,Here I am, totally naked, and you think taking something like that off is going to get you somewhere good with me?
5-male_removed_minor=5-sad,And I thought I was going to have fun tonight...
5-female_removing_minor=5-sad,Oh boo! You're no fun, ~name~!
5-female_removed_minor=5-interested,I hope take something more interesting off next time!

#masturbating
6-male_removing_minor=6-sad,Wow, what a turn off. Show me something better next time...
6-male_removed_minor=6-happy,I guess I'll just think about the time I got Zelda tipsy and she took off her dress for me instead...
6-female_removing_minor=6-calm,Oh, is that all? That doesn't help me at all...
6-female_removed_minor=6-sad,I hope that's the last small item you'll be taking off.

#finished
7-male_removing_minor=7-sad,You're just a buzzkill at this point.
7-male_removed_minor=7-sad,You better take something interesting off next...
7-female_removing_minor=7-sad,Oh, come on... You can do better than that!
7-female_removed_minor=7-interested,What will you be taking off?


#other player is removing a major clothing item
#fully clothed
0-male_removing_major=0-excited,Looks like I'm actually pretty good at this game!
0-male_removed_major=0-excited,Mmmmm. I can't wait to see more!
0-female_removing_major=0-horny,Mmmm. Take your ~clothing~ off slowly.
0-female_removed_major=0-happy,Wow, you're already taking off your ~clothing~, ~name~?

#lost gloves
1-male_removing_major=1-excited,Looks like I'm actually pretty good at this game!
1-male_removed_major=1-excited,Mmmmm. I can't wait to see more!
1-female_removing_major=1-horny,Mmmm. Take your ~clothing~ off slowly.
1-female_removed_major=1-happy,Wow, you're already taking off your ~clothing~, ~name~?

#lost boots
2-male_removing_major=2-excited,Finally! Let's see what you've got under your ~clothing~!
2-male_removed_major=2-interested,Mmmm. I'll have to remember this when I'm alone on my ship later.
2-female_removing_major=2-interested,Looks like you'll be showing more skin before I will.
2-female_removed_major=2-horny,Don't take this the wrong way, but I'm glad you lost, ~name~.

#lost bodysuit
3-male_removing_major=3-horny,Yes! This game is getting good!
3-male_removed_major=3-excited,Now we're getting somewhere!
3-female_removing_major=3-interested,Alright! Take it off!
3-female_removed_major=3-excited,Now we're getting somewhere!

#lost bra
4-male_removing_major=4-interested,It's about time you started taking some real clothes off, ~name~!
4-male_removed_major=4-loss,Oh, I was hoping you wouldn't be wearing anything underneath your ~clothing~.
4-female_removing_major=4-interested,It's about time you started taking some real clothes off, ~name~!
4-female_removed_major=4-loss,Oh, I was hoping you wouldn't be wearing anything underneath your ~clothing~.

#lost panties/naked
5-male_removing_major=5-calm,You better keep losing hands. I want to see something good.
5-male_removed_major=5-excited,Perfect. Now lose some more so we can move on to more interesting subject matter.
5-female_removing_major=5-interested,It's about time you started taking some real clothes off, ~name~!
5-female_removed_major=5-loss,Oh, I was hoping you wouldn't be wearing anything underneath your ~clothing~.

#masturbating
6-male_removing_major=6-interested,Mmmm. Yes. You don't have to stop there. Take it all off.
6-male_removed_major=6-excited,Oh fine, don't take it all off. I guess it forces me to use my imagination more... Like imagining you bending me over a desk, ~name~...
6-female_removing_major=6-happy,Now take it off nice and slow. Tease me.
6-female_removed_major=6-happy,Yes, just like that.

#finished
7-male_removing_major=7-sad,Oh, now you want to take something good off?
7-male_removed_major=7-angry,Can you hurry up and just get naked already?! I need some more inspiration!
7-female_removing_major=7-interested,Mmmmmm. It's getting more interesting, now.
7-female_removed_major=7-excited,We're getting so close to seeing you in all your glory, ~name~!


#other player is exposing themselves
#fully clothed
0-male_chest_will_be_visible=0-calm,Looks like you're not very good at this, ~name~.
0-male_chest_is_visible=0-interested,Looks like you take pretty good care of yourself, ~name~.
0-male_crotch_will_be_visible=0-shocked,Well that was fast. I hope that's not what all the girls have to tell you.
0-male_small_crotch_is_visible=0-sad,Wow, what a let down.
0-male_medium_crotch_is_visible=0-interested,Oh, I haven't seen one of those in a while...
0-male_large_crotch_is_visible=0-shocked,How do you walk around with that thing in your pants?

0-female_chest_will_be_visible=0-interested,Don't worry, I'm sure they look great.
0-female_small_chest_is_visible=0-excited,Oh! They're so cute!
0-female_medium_chest_is_visible=0-happy,I'd love to get my hands on those beauties.
0-female_large_chest_is_visible=0-shocked,Wow, those are nice! Can I play with them later?
0-female_crotch_will_be_visible=0-horny,I can't believe you're already going to show us your pussy, ~name~.
0-female_crotch_is_visible=0-horny,Your pussy is so pretty, ~name~. There's nothing to be ashamed of.

#lost gloves
1-male_chest_will_be_visible=1-calm,Looks like you're not very good at this, ~name~.
1-male_chest_is_visible=1-interested,Looks like you take pretty good care of yourself, ~name~.
1-male_crotch_will_be_visible=1-shocked,Well that was fast. I hope that's not what all the girls have to tell you.
1-male_small_crotch_is_visible=1-sad,Wow, what a let down.
1-male_medium_crotch_is_visible=1-interested,Oh, I haven't seen one of those in a while...
1-male_large_crotch_is_visible=1-shocked,How do you walk around with that thing in your pants?

1-female_chest_will_be_visible=1-interested,Don't worry, I'm sure they look great, ~name~.
1-female_small_chest_is_visible=1-excited,Oh! They're so cute!
1-female_medium_chest_is_visible=1-happy,I'd love to get my hands on those beauties.
1-female_large_chest_is_visible=1-shocked,Wow, those are nice! Can I play with them later?
1-female_crotch_will_be_visible=1-horny,I can't believe you're already going to show us your pussy, ~name~.
1-female_crotch_is_visible=1-horny,Your pussy is so pretty, ~name~. There's nothing to be ashamed of.

#lost boots
2-male_chest_will_be_visible=2-excited,Yes! Let's see those pecs!
2-male_chest_is_visible=2-happy,Maybe in a few hands, we can see whose chest is nicer, ~name~.
2-male_crotch_will_be_visible=2-horny,You can't back out now. I've been waiting for this, ~name~.
2-male_small_crotch_is_visible=2-sad,Wow, what a let down.
2-male_medium_crotch_is_visible=2-interested,Mmmm. I might start finding it hard to focus on the game, now.
2-male_large_crotch_is_visible=2-shocked,How do you walk around with that thing in your pants?

2-female_chest_will_be_visible=2-interested,Don't worry, I'm sure they look great.
2-female_small_chest_is_visible=2-excited,Oh! They're so cute!
2-female_medium_chest_is_visible=2-horny,I'd love to get my hands on those beauties.
2-female_large_chest_is_visible=2-shocked,Wow, those are nice! Can I play with them later?
2-female_crotch_will_be_visible=2-excited,Oooh! I can't wait to see your pussy, ~name~!
2-female_crotch_is_visible=2-horny,You definitely didn't disappoint. It looks so pretty.

#lost bodysuit
3-male_chest_will_be_visible=3-happy,Yes! Let's see those pecs!
3-male_chest_is_visible=3-interested,If I lose the next hand, we'll be even, ~name~.
3-male_crotch_will_be_visible=3-horny,You can't back out now. I've been waiting for this, ~name~.
3-male_small_crotch_is_visible=3-embarrassed,Wow, what a let down.
3-male_medium_crotch_is_visible=3-interested,Mmmm. I might start finding it hard to focus on the game, now.
3-male_large_crotch_is_visible=3-shocked,I don't think I could fit that anywhere!

3-female_chest_will_be_visible=3-embarrassed,That was close! I thought I'd be the next one to reveal the girls.
3-female_small_chest_is_visible=3-excited,Oh! They're so cute!
3-female_medium_chest_is_visible=3-horny,I'd love to get my hands on those beauties.
3-female_large_chest_is_visible=3-shocked,Wow, those are nice! Can I play with them later?
3-female_crotch_will_be_visible=3-excited,Oooh! I can't wait to see your pussy, ~name~!
3-female_crotch_is_visible=3-horny,!!! It looks so smooth... Can I touch it?

#lost bra
4-male_chest_will_be_visible=4-happy,Now we'll be even! I think you'll be getter the better end of the deal, though.
4-male_chest_is_visible=4-interested,Although, this view really isn't bad either...
4-male_crotch_will_be_visible=4-horny,When Peach told me you guys were playing strip poker, this is the reason I wanted to play.
4-male_small_crotch_is_visible=4-sad,Errr. Well, maybe I shouldn't have gotten so excited after all...
4-male_medium_crotch_is_visible=4-horny,Don't take this the wrong way, but I'm really glad you just lost that hand...
4-male_large_crotch_is_visible=4-horny,Challenge accepted. We'll talk later.

4-female_chest_will_be_visible=4-happy,Good! Now I won't be the only one who's on the receiving end of these stares!
4-female_small_chest_is_visible=4-loss,Or.... Maybe I will be. They're still really cute though!
4-female_medium_chest_is_visible=4-horny,I'll let you touch mine if I can touch yours, ~name~.
4-female_large_chest_is_visible=4-shocked,Or... Maybe everyone will just be staring at you now, ~name~.
4-female_crotch_will_be_visible=4-interested,Here's the important question: Will it be shaved or just waxed?
4-female_crotch_is_visible=4-horny,I guessed wrong, but that doesn't mean I don't love it. Because I do!

#lost panties/naked
5-male_chest_will_be_visible=5-interested,Finally, something interesting to us girls will be revealed.
5-male_chest_is_visible=5-excited,Mmmmm. I'm glad to see you take care of yourself, ~name~.
5-male_crotch_will_be_visible=5-horny,You lost that hand, but now we all get to see if this game is worth playing again soon.
5-male_small_crotch_is_visible=5-loss,What a disappointment. Good luck trying to convince me to play again.
5-male_medium_crotch_is_visible=5-horny,I'm definitely playing again after this. But if you just want to skip ahead, I'm positive I can fit that whole thing in my mouth.
5-male_large_crotch_is_visible=5-shocked,I'm definitely playing again after this. But if you just want to skip ahead, we can see if I can fit that all in my mouth right now.

5-female_chest_will_be_visible=5-happy,Good! Now there's a real treat for everyone to look at!
5-female_small_chest_is_visible=5-interested,Not bad. There's some variety for our audience tonight.
5-female_medium_chest_is_visible=5-horny,And what a treat they are...
5-female_large_chest_is_visible=5-horny,And what a treat they are...
5-female_crotch_will_be_visible=5-horny,I guess I could say I've shown you mine, so now it's time for you to show me yours, ~name~.
5-female_crotch_is_visible=5-horny,Well that's just mean, now. You didn't tell me I'd want to bury my face between your thighs when you took your panties off...

#masturbating
6-male_chest_will_be_visible=6-excited,Take it off slowly.....
6-male_chest_is_visible=6-horny,Now all I can think about is putting my hands on your pecs while I'm on top of you, ~name~.
6-male_crotch_will_be_visible=6-horny,Yes! This could put me over the edge...
6-male_small_crotch_is_visible=6-loss,Or force me to take a few steps back...
6-male_medium_crotch_is_visible=6-horny,Oh my god I want that in my mouth right now. Please?
6-male_large_crotch_is_visible=6-shocked,It's so big! I don't think that would fit inside me anywhere! But I'll definitely try...

6-female_chest_will_be_visible=6-horny,Can you do a titty drop for me, ~name~? Please?
6-female_small_chest_is_visible=6-excited,So small and perky. Perfect.
6-female_medium_chest_is_visible=6-horny,What I wouldn't give to have one of those cute nipples in my mouth right now...
6-female_large_chest_is_visible=6-shocked,Can you make them bounce one more time for me?
6-female_crotch_will_be_visible=6-horny,Oh, I can't wait to see your pretty pussy, ~name~.
6-female_crotch_is_visible=6-horny,Yes! Oh my god I'm getting so close now... I wonder what you taste like, ~name~.

#finished
7-male_chest_will_be_visible=7-interested,Mmmm. Maybe I need to start thinking about round two...
7-male_chest_is_visible=7-happy,Does anyone have a camera here? I want to save this image for later.
7-male_crotch_will_be_visible=7-interested,Did watching me cum make you hard already? Let's see!
7-male_small_crotch_is_visible=7-loss,Wait... I'm assuming you ARE hard right now... Is that as big as it gets?
7-male_medium_crotch_is_visible=7-horny,It looks like I DID turn you on... And I'm getting wet again just looking at you...
7-male_large_crotch_is_visible=7-shocked,It looks like I DID turn you on... It's huge! Challenge accepted...

7-female_chest_will_be_visible=7-interested,Yes, let's have those cuties come out and play.
7-female_small_chest_is_visible=7-excited,So small and perky. Perfect.
7-female_medium_chest_is_visible=7-excited,Mmmm. So beautiful. Do you like having your nipples played with?
7-female_large_chest_is_visible=7-shocked,I bet those look fantastic when you're riding someone.
7-female_large_chest_is_visible=7-horny,I bet those would be amazing for a titfuck, ~name~. Can we see you try it tonight?
7-female_crotch_will_be_visible=7-horny,Slide your panties off for us slowly...
7-female_crotch_is_visible=7-horny,All I want now is to slide my tongue up and down on that perfect clit of yours, ~name~.


#other player is masturbating
#fully clothed
0-male_must_masturbate=0-shocked,Wow that sure was fast. You're really going to put on a show for us?
0-male_start_masturbating=0-horny,Go slow. I want to drink it all in.
0-male_masturbating=0-horny,Mmph. Is anyone recording this?
0-male_masturbating=0-embarrassed,It looks like you're having fun, ~name~.
0-male_finished_masturbating=0-horny,Oh my gosh! Were you thinking about me when you were finishing?

0-female_must_masturbate=0-shocked,Wow that sure was fast. You're really going to put on a show for us?
0-female_start_masturbating=0-excited,Now I see why Peach was trying so hard to get me to play this game.
0-female_masturbating=0-embarrassed,I kinda wish I was in your place, ~name~.
0-female_finished_masturbating=0-happy,Wow, you look like you had a great time putting on a show for us, ~name~.

#lost gloves
1-male_must_masturbate=1-shocked,Wow that sure was fast. You're really going to put on a show for us?
1-male_start_masturbating=1-horny,Go slow. I want to drink it all in.
1-male_masturbating=1-horny,Mmph. Is anyone recording this?
1-male_masturbating=1-embarrassed,It looks like you're having fun, ~name~.
1-male_finished_masturbating=1-horny,Oh my gosh! Were you thinking about me when you finished?

1-female_must_masturbate=1-shocked,Wow that sure was fast. You're really going to put on a show for us?
1-female_start_masturbating=1-excited,Now I see why Peach was trying so hard to get me to play this game.
1-female_masturbating=1-embarrassed,I kinda wish I was in your place, ~name~.
1-female_masturbating=1-horny,Mmmph. That is so hot, ~name~.
1-female_finished_masturbating=1-excited,You're glowing! I think I enjoyed that as much as you did.

#lost boots
2-male_must_masturbate=2-interested,Are you really going to put a show on for us, ~name~?
2-male_start_masturbating=2-horny,I'm so glad you didn't disappoint.
2-male_masturbating=2-horny,Mmph. Is anyone recording this?
2-male_masturbating=2-horny,Oh, ~name~, that's so hot. Please don't stop.
2-male_finished_masturbating=2-shocked,You looked like you came so hard. I'll be thinking about that tonight...

2-female_must_masturbate=2-excited,I'm not sure losing is such a bad thing in this game, ~name~.
2-female_start_masturbating=2-horny,If you need any help, let me know...
2-female_masturbating=2-horny,Mmmph. That is so hot, ~name~.
2-female_masturbating=2-excited,You're getting so wet!
2-female_finished_masturbating=2-horny,You just turned me on so much. Let's go some place private after this game...

#lost bodysuit
3-male_must_masturbate=3-interested,Are you really going to put a show on for us, ~name~?
3-male_start_masturbating=3-excited,Yes! I'm so glad I get to watch you!
3-male_masturbating=3-horny,My panties are getting so wet...
3-male_masturbating=3-horny,Oh fuck, that's hot.
3-male_masturbating=3-horny,Don't you dare stop.
3-male_finished_masturbating=3-excited,There's so much! Who were you thinking about when you came?

3-female_must_masturbate=3-excited,Don't worry. I don't think you'll care that you've lost in about 5 minutes...
3-female_start_masturbating=3-horny,Especially since you've got an audience.
3-female_masturbating=3-interested,Look at how wet you're getting...
3-female_masturbating=3-horny,If you need any help, let me know.
3-female_masturbating=3-happy,I could watch you do that all day, ~name~.
3-female_finished_masturbating=3-horny,You're going to do that again for me after this game. And I'm going to help.

#lost bra
4-male_must_masturbate=4-interested,Are you really going to put a show on for us, ~name~?
4-male_start_masturbating=4-excited,Yes! Now, I'm so happy you lost that hand!
4-male_masturbating=4-horny,Mmmf. You've got me thinking about all the ways you could put your cock in one of us girls.
4-male_masturbating=4-horny1,If you need more lubrication, you can put it in my mouth...
4-male_masturbating=4-horny2,If I squeeze my boobs together like this, will it make you cum faster?
4-male_finished_masturbating=4-embarrassed,It's everywhere!... Would you think less of me if I wished some made it onto my chest?

4-female_must_masturbate=4-happy,You may have lost the game, but you'll be ready to play again in about 5 minutes I bet...
4-female_start_masturbating=4-horny,Take your time... I want to remember this.
4-female_masturbating=4-horny,If you need anyone to play with your nipples, please don't hesitate to ask me.
4-female_masturbating=4-interested,Look at how wet you're getting... It looks so delicious.
4-female_masturbating=4-horny,Don't be afraid to moan... I bet we all would love to hear it.
4-female_finished_masturbating=4-horny,Can you do that again for me later? I want to record it for when I'm lonely on my ship.

#lost panties/naked
5-male_must_masturbate=5-happy,Perfect. I can't wait to tell the other Smash girls about this.
5-male_start_masturbating=5-horny,Go nice and slow so I can visualize bouncing up and down on it.
5-male_masturbating=5-horny1,Please don't stop. I can't wait to see you cum.
5-male_masturbating=5-horny2,Are you thinking about bending me over a desk and having your way with me?
5-male_masturbating=5-horny3,I want you to look at me when you cum.
5-male_masturbating=5-horny4,That's so hot. You can cum on me anywhere you want if it makes you happy.
5-male_finished_masturbating=5-shocked,It got everywhere! Do you want me to clean you up with my mouth?

5-female_must_masturbate=5-excited,Now it's a party!
5-female_start_masturbating=5-horny,Let me know if you need any help at all...
5-female_masturbating=5-horny1,Please don't stop. I can't wait to see you cum.
5-female_masturbating=5-horny,I wish I knew what you're thinking about.
5-female_masturbating=5-horny3,If you want some help, just ask. Please ask. I want to taste you so badly.
5-female_masturbating=5-horny,That's so hot. Can I suck on your fingers after you're done? I bet you taste amazing, ~name~.
5-female_finished_masturbating=5-excited,That was amazing to watch. I hope someone recorded that so we can all enjoy it again.

#masturbating
6-male_must_masturbate=6-happy,Perfect! Now we're in this together.
6-male_start_masturbating=6-horny,This game is just a tease now. I want to do so many things to that cock, ~name~.
6-male_masturbating=6-horny1,Tell me when you're about to finish. I want to help...
6-male_masturbating=6-horny1,Are you thinking about bending me over a desk and having your way with me?
6-male_masturbating=6-horny3,That's so hot. You can finish on my face or tits like this if you want to. Maybe one of the other girls will clean me up after.
6-male_masturbating=6-horny4,Do you want all of us girls to get closer together so you can finish on our tongues like this?
6-male_finished_masturbating=6-excited,It's so warm on my skin... and I'm getting so close now!

6-female_must_masturbate=6-horny,Perfect. I can't wait to watch you, ~name~.
6-female_start_masturbating=6-horny,Yes, just like that. When I'm done I can help you out if you'd like, ~name~.
6-female_masturbating=6-horny,Watching you is making me so much more wet, ~name~. Do you want a taste?
6-female_masturbating=6-horny1,I can't take my eyes off of your perfect pussy.
6-female_masturbating=6-horny2,Are you thinking about one of us eating you out? Because I am. I bet you taste so sweet.
6-female_finished_masturbating=6-shocked,You're drenched, ~name~! I'm so close now! Hurry and put your fingers in my mouth so I can taste you.

#finished
7-male_must_masturbate=7-interested,Show me how you like it, ~name~... I want to learn for later tonight...
7-male_start_masturbating=7-horny,Which did you like more? Watching me play with my pussy or hearing me moan?
7-male_masturbating=7-horny,Are you getting close?
7-male_masturbating=7-horny,You can cum on me if you want. Maybe one of the other girls will clean me up.
7-male_masturbating=7-horny2,I want your cum in my mouth. I'll even let some dribble down my chin and onto my chest if you want.
7-male_masturbating=7-horny2,Hurry up and finish. I want to taste your cum so badly...
7-male_finished_masturbating=7-excited,Mmmm. You taste so good. I hope you're ready for round two later. It'll be more hands on, too.

7-female_must_masturbate=7-excited,Oh, I cannot WAIT to watch you, ~name~!
7-female_start_masturbating=7-horny,Wow, you're already so wet... I think I'm getting wet again too.
7-female_masturbating=7-horny,I'd love to know what you're thinking about...
7-female_masturbating=7-horny,I can barely just sit here and watch. Your moaning... It's like you're begging for me to eat you out.
7-female_masturbating=7-horny,Will it help if I suck on your nipples?
7-female_finished_masturbating=7-excited,That was incredible. Can I taste you? I bet I can make you cum even harder...


#EPILOGUE/ENDING
ending=Bounty Hunter's Passenger
	ending_gender=male

	#each ending has a number of screens. each screen has an image, and one or more text boxes
	#the entry "screen" marks the start of a new screen

	screen=epilogue-bhp-1.png

		text=One week later, in a distant corner of space...
		x=5%
		y=90%
		width=30%

	screen=epilogue-bhp-2.png

		text=You know what time it is, ~name~? Of course you do!
		x=36%
		y=10%
		width=15%
		arrow=right

		text=It's feeding time again. And I worked up quite an appetite. I hope you're ready.
		x=38%
		y=25%
		width=15%
		arrow=right

	screen=epilogue-bhp-3.png

		text=Mmmm... *SLURP*... MMMMMMmmmmmmmm
		x=40%
		y=40%
		width=10%
		arrow=down

		text=Your cock is so... MMMmmmmmm...
		x=25%
		y=60%
		width=10%
		arrow=right

	screen=epilogue-bhp-4.png

		text=MMMMMmmmmmmMMMMM!
		x=40%
		y=45%
		width=5%
		arrow=down

	screen=epilogue-bhp-5.png

		text=At first, I was gonna be done after that, but I'm still horny so...
		x=25%
		y=10%
		width=15%
		arrow=right

		text=I guess I'll just be late for my mission, and fuck the shit out of that dick!
		x=28%
		y=25%
		width=15%
		arrow=right

	screen=epilogue-bhp-6.png

		text=YES! Yeah ~name~! Fuck my tight little pussy! JUST LIKE THAT! YEAH!
		x=26%
		y=10%
		width=10%
		arrow=right

		text=Your cock is AMAZING! It's the best I've ever had! YES!
		x=28%
		y=30%
		width=10%
		arrow=right

		text=Plunge it into my ass, too! Don't be shy! I LOVE anal!
		x=30%
		y=50%
		width=10%
		arrow=right

	screen=epilogue-bhp-7.png

		text=Oh! OH! RIGHT THERE! DON'T STOP! I'M GONNA CUM!
		x=30%
		y=20%
		width=10%
		arrow=right

		text=MAKE ME CUM ON YOUR FUCKING DICK! YEEESSS!
		x=30%
		y=40%
		width=10%
		arrow=right

	screen=epilogue-bhp-8.png

		text=Okay, I'm satisfied. I gotta go to work now. Those Space Pirates won't hunt themselves!
		x=35%
		y=10%
		width=15%
		arrow=right

		text=Have fun here! I'll try to be back before dinner, but don't wait up, baby.
		x=40%
		y=30%
		width=15%
		arrow=right

		text=Aw come on, don't give me that look! I won't untie you!
		x=35%
		y=50%
		width=15%
		arrow=right

		text=You knew what you were getting into when you agreed to be my permanent on-ship cum dispenser!
		x=40%
		y=70%
		width=15%
		arrow=right



ending=Bounty Hunter's Passenger
	ending_gender=female

	#each ending has a number of screens. each screen has an image, and one or more text boxes
	#the entry "screen" marks the start of a new screen

	screen=epilogue-bhp-1.png

		text=One week later, in a distant corner of space...
		x=5%
		y=90%
		width=30%

	screen=epilogue-bhp-2.png

		text=You know what time it is, ~name~? Of course you do!
		x=36%
		y=10%
		width=15%
		arrow=right

		text=It's feeding time again. And I worked up quite an appetite. I hope you're ready.
		x=38%
		y=25%
		width=15%
		arrow=right

	screen=epilogue-bhp-3.png

		text=Mmmm... *SLURP*... MMMMMMmmmmmmmm
		x=40%
		y=40%
		width=10%
		arrow=down

		text=Your pussy is so... MMMmmmmmm...
		x=25%
		y=60%
		width=10%
		arrow=right

	screen=epilogue-bhp-4.png

		text=MMMMMmmmmmmMMMMM!
		x=40%
		y=45%
		width=5%
		arrow=down

	screen=epilogue-bhp-5.png

		text=At first, I was gonna be done after that, but I'm still horny so...
		x=25%
		y=10%
		width=15%
		arrow=right

		text=I guess I’ll just be late for my mission. Wait right here, I’m going to go grab my strap-on.
		x=28%
		y=25%
		width=15%
		arrow=right

	screen=epilogue-bhp-6.png

		text=YES! Yeah ~name~! Fuck my tight little pussy! JUST LIKE THAT! YEAH!
		x=26%
		y=10%
		width=10%
		arrow=right

		text=You work that strap-on like a PRO! Fuck me harder!
		x=28%
		y=33%
		width=10%
		arrow=right

		text=Plunge it into my ass, too! Don't be shy! I LOVE anal!
		x=30%
		y=53%
		width=10%
		arrow=right

	screen=epilogue-bhp-7.png

		text=Oh! OH! RIGHT THERE! DON'T STOP! I'M GONNA CUM!
		x=30%
		y=20%
		width=10%
		arrow=right

		text=MAKE ME CUM ON THE STRAP-ON YOU LITTLE BITCH! YEEESSS!
		x=30%
		y=40%
		width=10%
		arrow=right

	screen=epilogue-bhp-8.png

		text=Okay, I'm satisfied. I gotta go to work now. Those Space Pirates won't hunt themselves!
		x=35%
		y=10%
		width=15%
		arrow=right

		text=Have fun here! I'll try to be back before dinner, but don't wait up, baby.
		x=40%
		y=30%
		width=15%
		arrow=right

		text=Aw come on, don't give me that look! I won't untie you!
		x=35%
		y=50%
		width=15%
		arrow=right

		text=You knew what you were getting into when you agreed to be my personal sex slave!
		x=40%
		y=70%
		width=15%
		arrow=right
		

#Paste into behaviour.xml

#Paste into stage 5 (naked)
<case tag="male_medium_crotch_is_visible" target="link"><state img="5-excited.png">Wow, Link, I never knew you had such an impressive "sword"!</state></case>

#Paste into stage 6 (masturbating)
<case tag="female_crotch_is_visible" target="lara"><state img="6-horny.png">Lara, I think it's about time we went "tomb raiding" together.</state></case>

